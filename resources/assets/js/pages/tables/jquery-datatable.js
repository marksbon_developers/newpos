$(function () {
    /*$.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        responsive: true,
        columnDefs: [{
            orderable: false,
            width: '100px'
        }],
        dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Search:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });*/

    // New Category Datatable
    $('#newCategory').DataTable({
        autoWidth: false,
        order: false,
        ajax: {
            type : 'GET',
            url : domain + 'category/getall',
            dataSrc: '',
            error: function() {
                alert("Error Pae");
            }
        },
        columns: [
            {data: "id"},
            {data: "name"},
            {data: "status", render: function(data,type,row,meta) {
                if(row.status == "active") {
                    label_class = "label-success";
                }
                else if(row.status == "inactive"){
                    label_class = "label-danger";
                }
                user_status = row.status;
                return '<span class="label '+label_class+'">'+row.status+'</span>';}
            },
            {render: function (data, type, row, meta) {
                if(row.status == "active") {
                    status = "inactive";
                    btn_status = "btn-warning btn-xs";
                    btn_text = "DeActivate";
                }
                else {
                    status = "active";
                    btn_status = "btn-success btn-xs";
                    btn_text = "Activate";
                }

                return '<form action="'+domain+'category/status" method="post">' +
                    '<input type="hidden" name="category_id" value="'+row.id+'">' +
                    '<input type="hidden" name="status" value="'+status+'">' +
                    '<button name="statusCategorySubmit" class="'+btn_status+'">'+btn_text+'</button>' +
                    '</form>';
            }}
        ]
    });

    // New Supplier Datatable
    $('#newSupplier').DataTable({
        autoWidth: false,
        order: false,
        ajax: {
            type : 'GET',
            url : domain + 'supplier/getall',
            dataSrc: '',
            error: function() {
                alert("Error Pae");
            }
        },
        columns: [
            /*{data: "id"},*/
            {data: "name"},
            {data: "contact"},
            {data: "status", render: function(data,type,row,meta) {
                if(row.status == "active") {
                    label_class = "label-success";
                }
                else if(row.status == "inactive"){
                    label_class = "label-danger";
                }
                user_status = row.status;
                return '<span class="label '+label_class+'">'+row.status+'</span>';}
            },
            {render: function (data, type, row, meta) {
                if(row.status == "active") {
                    status = "inactive";
                    btn_status = "btn-warning btn-xs";
                    btn_text = "DeActivate";
                }
                else {
                    status = "active";
                    btn_status = "btn-success btn-xs";
                    btn_text = "Activate";
                }

                return '<form action="'+domain+'supplier/status" method="post">' +
                    '<input type="hidden" name="supplier_id" value="'+row.id+'">' +
                    '<input type="hidden" name="status" value="'+status+'">' +
                    '<button name="statusSupplierSubmit" class="'+btn_status+'">'+btn_text+'</button>' +
                    '</form>';
            }}
        ]
    });

    // New Supplier Datatable
    $('#productDetails').DataTable({
        autoWidth: false,
        order: false,
        ajax: {
            type : 'GET',
            url : domain + 'products/getall',
            dataSrc: '',
            error: function() {
                alert("Error Pae");
            }
        },
        columns: [
            {data: "name"},
            {data: "category"},
            {data: "supplier"},
            {data: "unit_price"},
            {data: "reorder_level"},
            {data: "expiry_date"},
            {data: "status", render: function(data,type,row,meta) {
                if(row.status == "active") {
                    label_class = "label-success";
                }
                else if(row.status == "inactive"){
                    label_class = "label-danger";
                }
                user_status = row.status;
                return '<span class="label '+label_class+'">'+row.status+'</span>';
            }},
            {render: function (data, type, row, meta) {
                if(row.status == "active") {
                    status = "inactive";
                    btn_status = "btn-warning btn-xs";
                    btn_text = "DeActivate";
                }
                else {
                    status = "active";
                    btn_status = "btn-success btn-xs";
                    btn_text = "Activate";
                }

                return '<form action="'+domain+'products/status" method="post">' +
                    '<input type="hidden" name="product_id" value="'+row.id+'">' +
                    '<input type="hidden" name="status" value="'+status+'">' +
                    '<button name="statusProductSubmit" class="'+btn_status+'">'+btn_text+'</button>' +
                    '</form>';
            }}
        ]
    });

    //Exportable table
    $('.js-exportable').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
});