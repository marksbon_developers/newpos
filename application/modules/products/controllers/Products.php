<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MX_Controller
{
    /*******************************
      Constructor 
    *******************************/
    public function __construct() {
      parent::__construct();
    }

	 /*******************************
	 Create New
	  *******************************/
	 public function getall()
	 {
		  if(isset($_SESSION['user']['id'])) :
				# Loading Helper / Models
				$this->load->model('ProductModel');
				$query_result = $this->ProductModel->getAll();

				# data manipulation
				foreach($query_result as $key => $result)
				{
					 $query_result[$key]->unit_price = number_format($result->unit_price, 2);
				}

				print_r(json_encode($query_result));
		  else :
				print "";
		  endif;
	 }

    /*******************************
        Create New
    *******************************/
    public function store()
    {
        if(isset($_POST['newProductSubmit'])) {
            $this->form_validation->set_rules('name', 'Product Name', 'required|trim|is_unique[product_details.name]');
            $this->form_validation->set_rules('category', 'Category', 'required|trim');
            $this->form_validation->set_rules('supplier', 'Supplier', 'required|trim');
            $this->form_validation->set_rules('unit_qty', 'Unit Quantity', 'required|trim');
            $this->form_validation->set_rules('unit_price', 'Unit Price', 'required|trim');
            $this->form_validation->set_rules('reorder_level', 'Reorder Level', 'required|trim');
            $this->form_validation->set_rules('expiry_date', 'Exipry date', 'required|trim');

            if ($this->form_validation->run() === FALSE) {
                $this->session->set_flashdata('error', validation_errors());
                redirect('dashboard/newstock');
            }
            else {
                # Loading Helper / Models
                $this->load->model('ProductModel');

					 $productData = [
						  'name' => $this->input->post('name'),
						  'category_id' => $this->input->post('category'),
						  'supplier_id' => $this->input->post('supplier'),
						  'unit_qty' => $this->input->post('unit_qty'),
						  'unit_price' => $this->input->post('unit_price'),
						  'reorder_level' => $this->input->post('reorder_level'),
						  'expiry_date' => $this->input->post('expiry_date'),
					 ];
                $response = $this->ProductModel->createNew($productData);

                if($response)
                    $this->session->set_flashdata('success', "Product Added Successful");
                else
                    $this->session->set_flashdata('error', "Product Add Failed");

                redirect('dashboard/newstock');
            }
        }
        else
            redirect('dashboard');
    }

	 /*******************************
	 	Edit Category
	  ******************************/
	 public function update()
	 {
		  if(isset($_POST['updateCategorySubmit'])) {
				$this->form_validation->set_rules('category_id', 'Category ID', 'required|trim');
				$this->form_validation->set_rules('name', 'Category Name', 'required|trim|is_unique[product_category.name]');

				if ($this->form_validation->run() === FALSE) {
					 $this->session->set_flashdata('error', validation_errors());
					 redirect('dashboard/newstock');
				}
				else {
					 # Loading Helper / Models
					 $this->load->model('CategoryModel');

					 $updateData = [
					 	 'name' => $this->input->post('name'),
						  'updated_by' => $_SESSION['user']['id']
					 ];
					 $categoryID = $this->input->post('category_id');

					 $response = $this->CategoryModel->updateCategory($updateData,$categoryID);

					 if($response)
						  $this->session->set_flashdata('success', "Category Updated");
					 else
						  $this->session->set_flashdata('error', "Update Failed");

					 redirect('dashboard/newstock');
				}
		  }
		  else
				redirect('dashboard');
	 }

	 /*******************************
	 Edit Status
	  ******************************/
	 public function status()
	 {
		  if(isset($_POST['statusProductSubmit'])) {
				$this->form_validation->set_rules('product_id', 'Product', 'required|trim');
				$this->form_validation->set_rules('status', 'Supplier Status', 'required|trim');

				if ($this->form_validation->run() === FALSE) {
					 $this->session->set_flashdata('error', validation_errors());
					 redirect('dashboard/newstock');
				}
				else {
					 # Loading Helper / Models
					 $this->load->model('ProductModel');

					 $updateData = [
						  'status' => $this->input->post('status'),
						  'updated_by' => $_SESSION['user']['id']
					 ];
					 $updateID = $this->input->post('product_id');
					 $response = $this->ProductModel->update($updateData,$updateID);

					 if($response)
						  $this->session->set_flashdata('success', "Product Status Updated");
					 else
						  $this->session->set_flashdata('error', "Update Failed");

					 redirect('dashboard/newstock');
				}
		  }
		  else
				redirect('dashboard');
	 }
}//End of Class
