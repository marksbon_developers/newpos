<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends MX_Controller
{
    /*******************************
      Constructor 
    *******************************/
    public function __construct() {
      parent::__construct();
    }

	 /*******************************
		  Retrieve All
	  *******************************/
	 public function getall()
	 {
		  if(isset($_SESSION['user']['id'])) :
		  	 	# Loading Helper / Models
				$this->load->model('SupplierModel');
		  		$query_result = $this->SupplierModel->getAllSupplier();

				print_r(json_encode($query_result));
		  else :
				print "";
		  endif;
	 }

    /*******************************
        Create New
    *******************************/
    public function store()
    {
        if(isset($_POST['newSupplierSubmit'])) {
            $this->form_validation->set_rules('name', 'Supplier Name', 'required|trim|is_unique[product_suppliers.name]');
            $this->form_validation->set_rules('contact', 'Supplier Contact', 'required|trim|is_unique[product_suppliers.contact]');

            if ($this->form_validation->run() === FALSE) {
                $this->session->set_flashdata('error', validation_errors());
                redirect('dashboard/newstock');
            }
            else {
                # Loading Helper / Models
                $this->load->model('SupplierModel');

					 $supplierData = [
					 	 'name' => $this->input->post('name'),
						  'contact' => $this->input->post('contact')
					 ];
                $response = $this->SupplierModel->newSupplier($supplierData);

                if($response)
                    $this->session->set_flashdata('success', "Category Added Successful");
                else
                    $this->session->set_flashdata('error', "Category Add Failed");

                redirect('dashboard/newstock');
            }
        }
        else
            redirect('dashboard');
    }

	 /*******************************
	 	Update Info
	  ******************************/
	 public function update()
	 {
		  if(isset($_POST['updateCategorySubmit'])) {
				$this->form_validation->set_rules('category_id', 'Category ID', 'required|trim');
				$this->form_validation->set_rules('name', 'Category Name', 'required|trim|is_unique[product_category.name]');

				if ($this->form_validation->run() === FALSE) {
					 $this->session->set_flashdata('error', validation_errors());
					 redirect('dashboard/newstock');
				}
				else {
					 # Loading Helper / Models
					 $this->load->model('CategoryModel');

					 $updateData = [
					 	 'name' => $this->input->post('name'),
						  'updated_by' => $_SESSION['user']['id']
					 ];
					 $categoryID = $this->input->post('category_id');

					 $response = $this->CategoryModel->updateCategory($updateData,$categoryID);

					 if($response)
						  $this->session->set_flashdata('success', "Category Updated");
					 else
						  $this->session->set_flashdata('error', "Update Failed");

					 redirect('dashboard/newstock');
				}
		  }
		  else
				redirect('dashboard');
	 }

	 /*******************************
	 Edit Status
	  ******************************/
	 public function status()
	 {
		  if(isset($_POST['statusSupplierSubmit'])) {
				$this->form_validation->set_rules('supplier_id', 'Supplier', 'required|trim');
				$this->form_validation->set_rules('status', 'Supplier Status', 'required|trim');

				if ($this->form_validation->run() === FALSE) {
					 $this->session->set_flashdata('error', validation_errors());
					 redirect('dashboard/newstock');
				}
				else {
					 # Loading Helper / Models
					 $this->load->model('SupplierModel');

					 $updateData = [
						  'status' => $this->input->post('status'),
						  'updated_by' => $_SESSION['user']['id']
					 ];
					 $supplierID = $this->input->post('supplier_id');

					 $response = $this->SupplierModel->updateSupplier($updateData,$supplierID);

					 if($response)
						  $this->session->set_flashdata('success', "Supplier Status Updated");
					 else
						  $this->session->set_flashdata('error', "Update Failed");

					 redirect('dashboard/newstock');
				}
		  }
		  else
				redirect('dashboard');
	 }
}//End of Class
