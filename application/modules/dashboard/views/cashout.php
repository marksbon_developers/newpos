<section class="content profile-page">
    <div class="container-fluid">
        <div class="block-header text-right mb-0">
            
            <div role="tabpanel" class="tab-pane active in" id="income">
               
                <div class="row clearfix">
                    <div class="col-sm-12 col-md-12 col-lg-4">
                        <div class="card">
                            <div class="header">
                                <p class="text-left mb-0" style="font-size:20px">GH¢10.00</p>
                                <h2>Total Income<small >Monthly POS Accoumulated Amount</small></h2>
                               
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-4">
                        <div class="card">
                            <div class="header">
                                <p class="text-left mb-0" style="font-size:20px">GH¢-10.00</p>
                                <h2>Expenditures<small >Total expences inquired this Month</small></h2>
                               
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-4">
                        <div class="card">
                            <div class="header">
                                <p class="text-left mb-0" style="font-size:20px">GH¢10.00</p>
                                <h2>Bank Account<small >Deposit from POS</small></h2>
                               
                            </div>
                        </div>
                    </div>
                </div> 

            </div>
        </div>        
        <div class="row clearfix">
            <div class="col-lg-4 col-md-12 col-sm-12">
                
                <div class="card">
                    <div class="header">
                        <h2>CURRENT TRANSACTIONS</h2>
                        <p class="text-right mb-0"><sup>Total Transactions</sup></p>
                        <p class="text-right mb-0" style="font-size:30px">GH¢110.00</p>
                        <hr  style="margin-top: 5px;margin-bottom:1px " >
                    </div>
                    
                    <div class="body">
                         <strong>Bismark <sup style="font-size:10px">2/08/19</sup></strong></br>
                        <sup>Bank</sup>(Cash-In)<sup>Expenditure </sup>
                        <sup><i class="icon-arrow-left16 position-left"></i><p class="text-right mb-0" style="font-size:20px">¢10.00</p></sup>
                        <hr style="margin-top: 5px;margin-bottom:1px ">
                        <strong>Bismark <sup style="font-size:10px">4/08/19</sup></strong></br>
                        <sup>Expenditure</sup>(Cash-out)<sup>  For light bulbs</sup>
                        <sup><i class="icon-arrow-left16 position-left"></i><p class="text-right mb-0" style="font-size:20px">¢10.00</p></sup>
                        <hr style="margin-top: 5px;margin-bottom:1px ">
                         <strong>Osborn <sup style="font-size:10px">5/08/19</sup></strong></br>
                        <sup>Total Sales</sup>(Cash-out)<sup> Bank Deposit</sup>
                        <sup><p class="text-right mb-0" style="font-size:20px">¢100.00</p></sup>
                        <hr style="margin-top: 5px;margin-bottom:1px ">
                      
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body"> 
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item"><a class="nav-link active"data-toggle="tab"  href="#report">Cash Out</a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#timeline">Cash In</a></li>
                        </ul>
                        
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane in active" id="report">  
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-control-element ">
              <select class="custom-select margin-bottom-20" id="accesstype" name="accesstype">
                <option value="" disabled selected>Select Acount First </option>
                <option>Expenditure</option>
                <option>Income</option>
              </select>
            </div></br>
                                   <div class="form-group">
                                             <label style="margin-bottom: -15px;">Amount *</label>
                                             <div class="form-line">
                                             <input type="text" class="form-control" placeholder="Amount here">
                                            </div>
                                            </div>
                                <div class="form-group" >
                                     <label style="margin-bottom: -15px;">Comments/Narative *</label>
                                    <div class="form-line">
                                        <textarea rows="4" name="comments" class="form-control no-resize" placeholder="Please type what you want..."></textarea>
                                    </div>
                                </div>
                                <!--Funds transfer        -->
                               
                                </div>
                                 <div class="col-md-6">
                                    <!-- funds transfer -->
                                    <div id="Credit" class="form-group drop-custum focused" style="display: none;">
                                    <select id="Credit" class="form-control show-tick" name="Credit" >
                                        <option value="">-- Credit --</option>
                                        <option value="10">Expenditure</option>
                                        <option value="20">Bank</option>
                                    </select>
                                </div>
                                 <div class="form-group">
                                             <label style="margin-bottom: -15px;">Authorizer *</label>
                                             <div class="form-line">
                                             <input type="text" class="form-control" placeholder="Amount here">
                                            </div>
                                            </div>
                                    <div class="form-group">
                                         <label style="margin-bottom: -15px;">Issure *</label>
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Payee Name here">
                                    </div>
                                </div> 
                                 <div id="Priority" class="form-group drop-custum focused" style="display: none;">
                                    <select class="form-control show-tick" name="Priority">
                                        <option value="">-- Priority --</option>
                                        <option value="10">Heigher</option>
                                        <option value="20">low</option>
                                    </select>
                                </div>
                                <div id="Receipt" class="form-group" style="display: none;">
                                         <label style="margin-bottom: -15px;">Upload Receipt if Any </label>
                                    <div class="form-line">
                                        <input type="File" class="form-control" name="Receipt" >
                                    </div>
                                </div>

                                <button type="submit" name="login" class="btn bg-pink-200 btn-block">Submit </button>
                                 </div>
                            </div>      
                            </div>
                            <div role="tabpanel" class="tab-pane" id="timeline">
                                <div class="timeline-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                             <div class="form-group">
                                             <label style="margin-bottom: -15px;">Amount *</label>
                                             <div class="form-line">
                                             <input type="text" class="form-control" placeholder="Amount here">
                                            </div>
                                            </div>
                                            <div class="form-group drop-custum focused">
                                    <select class="custom-select margin-bottom-2">
                                        <option value="">-- Tendency --</option>
                                        <option value="10">Cash</option>
                                        <option value="20">Cheque</option>
                                    </select>
                                </div>
                                            <div class="form-group">
                                             <label style="margin-bottom: -15px;">Received By * </label>
                                             <div class="form-line">
                                             <input type="text" class="form-control" placeholder="Received By">
                                            </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group drop-custum focused">
                                    <select class="custom-select margin-bottom-2">
                                        <option value="">-- Account --</option>
                                        <option value="10">Expendeture</option>
                                        <option value="20">Bank Account</option>
                                    </select>
                                </div>
                                        <div class="form-group">
                                             <label style="margin-bottom: -15px;">Cheque Number By * </label>
                                             <div class="form-line">
                                             <input type="text" class="form-control" placeholder="Cashed By">
                                            </div>
                                            </div>
                                            <button type="submit" name="login" class="btn bg-pink-200 btn-block">Submit </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

