
<section class="content home">
    <div class="container-fluid">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item"><a class="nav-link " data-toggle="tab" href="#income"><i class="zmdi zmdi zmdi-case-download"></i> <span>Out & Limited Stock </span></a></li>
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#sales"><i class="zmdi zmdi-file-text"></i> <span> Stock Manager</span></a></li>
        </ul> 
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane page-calendar" id="income">
                <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2>Register Information <small>Bio Data</small> </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="zmdi zmdi-more-vert"></i></a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <span style="color:red;">*</span>
                                        <input type="text" class="form-control" placeholder="Date">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group drop-custum">
                                    <span style="color:red;">*</span>
                                    <select class="form-control show-tick">
                                        <option value="">Supplier</option>
                                        <option value="10">General</option>
                                        <option value="20">Depressants</option>
                                         <option value="20">Stimulants</option>
                                          <option value="20">Hallucinogens</option>
                                           <option value="20">Opioids</option>
                                            <option value="20">Inhalants</option>
                                            <option value="20">Cannabis</option>
                                            <option value="20">psychoactive</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <span style="color:red;">*</span>
                                    <div class="form-line">
                                        <input type="number" class="datepicker form-control" placeholder="Invoice #" data-dtp="dtp_UNcwJ">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <span style="color:red;">*</span>
                                    <div class="form-line">
                                        <input type="date" class="datepicker form-control" placeholder="Vendor's Name" data-dtp="dtp_UNcwJ">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                      <span style="color:red;">*</span>
                                    <div class="form-line">
                                        <input type="number" mini="0" class="form-control" placeholder="¢ Total cost">
                                    </div>
                                </div>
                            </div>
                            <table class="table table-bordered table-striped table-hover ">
                            <thead>
                                <tr>
                                    <th style="width:5%;">#</th>
                                    <th style="width:25%;">Item Name</th>
                                    <th style="width:25%;">Discription</th>
                                    <th style="width:10%;">Unit P</th>
                                    <th style="width:20%;">Total P </th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="text" class="form-control" placeholder="#"></td>
                                    <td>
                                        <select class="form-control show-tick">
                                        <option value="">Item Name</option>
                                        <option value="10">General</option>
                                        <option value="20">Depressants</option>
                                         <option value="20">Stimulants</option>
                                          <option value="20">Hallucinogens</option>
                                           <option value="20">Opioids</option>
                                            <option value="20">Inhalants</option>
                                            <option value="20">Cannabis</option>
                                            <option value="20">psychoactive</option>
                                    </select>
                                    </td>
                                    <td>
                                        <select class="form-control show-tick">
                                        <option value="">Description</option>
                                        <option value="10">General</option>
                                        <option value="20">Depressants</option>
                                         <option value="20">Stimulants</option>
                                          <option value="20">Hallucinogens</option>
                                           <option value="20">Opioids</option>
                                            <option value="20">Inhalants</option>
                                            <option value="20">Cannabis</option>
                                            <option value="20">psychoactive</option>
                                    </select>
                                    </td>
                                    <td><input type="text" class="form-control" placeholder="#"></td>
                                    <td><input type="text" class="form-control" placeholder="#"></td>
                                    <td><i class="zmdi zmdi-plus"></i></td>
                                </tr>
                                
                            </tbody>
                        </table>
                            <div class="col-sm-12">
                                <div class="row clearfix jsdemo-notification-button">
                                <button type="submit" class="btn btn-raised g-bg-cyan"data-color-name="bg-black">Update</button>
                                <button type="submit" class="btn btn-raised">Cancel</button> 
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                                
            </div>
            
            <div role="tabpanel" class="tab-pane active in" id="sales">
                <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2>Update Stock Manager</h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="zmdi zmdi-more-vert"></i></a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body table-responsive">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr class="bg-light-green">
                                    <th>Name</th>
                                    <th>Cartegory</th>
                                    <th>Date Updated</th>
                                    <th>Exp. Date </th>
                                    <th>Discount</th>
                                    <th>Surplier</th>
                                    <th>Qty</th>
                                    <th>L.Stock</th>
                                    <th>Unit P.</th>
                                     <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr class="bg-light-green">
                                     <th>Name</th>
                                    <th>Cartegory</th>
                                    <th>Date Updated</th>
                                    <th>Exp. Date </th>
                                    <th>Discount</th>
                                    <th>Surplier</th>
                                    <th>Qty</th>
                                    <th >L.Stock</th>
                                    <th>Unit P.</th>
                                     <th>Action</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <tr class="bg-light-green">
                                    <td>Tiger Nixon</td>
                                    <td>psychoactive</td>
                                    <td>2011/04/25</td>
                                    <td>2011/04/25</td>
                                    <td>61</td>
                                    <td>osons Chemist</td>
                                    <td>3</td>
                                    <td>0</td>
                                    <td>320</td>
                                    <td><i class="zmdi zmdi-file-text"></i></td>
                                </tr>
                                <tr class="bg-light-green">
                                    <td>Garrett Winters</td>
                                   <td>psychoactive</td>
                                    <td>2011/04/25</td>
                                    <td>2011/04/25</td>
                                    <td>61</td>
                                    <td>osons Chemist</td>
                                    <td>320</td>
                                    <td>0</td>
                                    <td>3</td>
                                    <td><i class="zmdi zmdi-file-text"></i></td>
                                </tr>
                                <tr class="bg-orange" >
                                    <td>Ashton Cox</td>
                                    <td>psychoactive</td>
                                    <td>2011/04/25</td>
                                    <td>2011/04/25</td>
                                    <td>61</td>
                                    <td>osons Chemist</td>
                                    <td>12</td>
                                    <td >12</td>
                                    <td>3</td>
                                    <td><i class="zmdi zmdi-file-text"></i></td>
                                </tr>
                                <tr class="bg-red">
                                    <td>Cedric Kelly</td>
                                    <td>psychoactive</td>
                                    <td>2011/04/25</td>
                                    <td>2011/04/25</td>
                                    <td>61</td>
                                    <td>osons Chemist</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>3</td>
                                    <td><i class="zmdi zmdi-file-text"></i></td>
                                </tr>
                               
                                <tr class="bg-light-green">
                                    <td>Donna Snider</td>
                                    <td>psychoactive</td>
                                    <td>2011/04/25</td>
                                    <td>2011/04/25</td>
                                    <td>61</td>
                                    <td>osons Chemist</td>
                                    <td>320</td>
                                    <td>0</td>
                                    <td>3</td>
                                    <td><i class="zmdi zmdi-file-text"></i></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
            </div>            
        </div>
    </div>
</section>