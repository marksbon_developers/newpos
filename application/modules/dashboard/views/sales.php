
<section class="content profile-page">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Daily Sales</h2>
            <small class="text-muted">Cash At Hand</small>
        </div>        
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2>Recent sales</h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="zmdi zmdi-more-vert"></i></a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="row sales-report">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <h2>Jan 2017</h2>
                                    <p>SALES REPORT</p>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <h1 class="text-right text-success m-t-20">¢4,231</h1>
                                </div>
                            </div>
                        <div class="body table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Contact Number</th>
                                        <th>Quantity</th>
                                        <th>DATE</th>
                                        <th>Unit P.</th>
                                        <th>PRICE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td class="txt-oflo">0244444444</td>
                                        <td>2 </td>
                                        <td class="txt-oflo">Feb 11, 2017</td>
                                        <td><span class="text-success">¢2</span></td>
                                        <td><span class="text-success">¢25</span></td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td class="txt-oflo">02444444444</td>
                                        <td>1</td>
                                        <td class="txt-oflo">March 29, 2017</td>
                                        <td><span class="text-info">$1234</span></td>
                                        <td><span class="text-info">$1234</span></td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td class="txt-oflo">024555555</td>
                                        <td>5</td>
                                        <td class="txt-oflo">April 21, 2017</td>
                                        <td><span class="text-info">$1234</span></td>
                                        <td><span class="text-danger">-$204</span></td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td class="txt-oflo">02444445556</td>
                                        <td>4</td>
                                        <td class="txt-oflo">Jun 22, 2017</td>
                                          <td><span class="text-success">$24</span></td>
                                        <td><span class="text-success">$24</span></td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td class="txt-oflo">02455622225</td>
                                        <td>6</td>
                                        <td class="txt-oflo">July 20, 2017</td>
                                          <td><span class="text-success">$24</span></td>
                                        <td><span class="text-success">$21</span></td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td class="txt-oflo">0245555412</td>
                                        <td>2 </td>
                                        <td class="txt-oflo">July 21, 2017</td>
                                          <td><span class="text-success">$24</span></td>
                                        <td><span class="text-danger">-$12</span></td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td class="txt-oflo">02444545522</td>
                                        <td>1</td>
                                        <td class="txt-oflo">July 21, 2017</td>
                                          <td><span class="text-success">$24</span></td>
                                        <td><span class="text-success">$54</span></td>
                                    </tr>
                                </tbody>
                            </table>
                            
                           <a href="<?=base_url()?>dashboard/printreport" target="_blink" class="btn btn-raised btn-success"><i class="zmdi zmdi-print"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>