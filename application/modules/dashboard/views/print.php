

<body onload="possubmit()">
    <div class="wrapper">
      <!-- Main content -->
      <div class="row">
                            <div class="col-12">
                                <div class="media">
                                    <img class="align-self-start mr-3" src="<?=base_url()?>resources/assets/images/logo-placeholder.jpg" width="70" alt="Swift">
                                    <div class="media-body">
                                        <h6>Invoice #:54667546<br>
                                        </h6>
                                    </div>
                                </div>
                                <hr>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <address>
                                    <strong>Wiamoase,</strong><br>
                                    Opp. st. Johns Sch.<br>
                                   Sold by:Bismark
                                </address>
                            </div>
                            <div class="col-md-6 col-sm-6 text-right">
                                <p><strong>Order Date: </strong> Jun 15, 2016</p>
                                <p class="mb-0"><strong>Payment : </strong> <span class="badge bg-orange">Cash</span></p>
                            </div>
                        </div>                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Qt</th>
                                        <th>Desc.</th>
                                        <th>Unit</th>
                                        <th>Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1
                                        </td>
                                        <td class="txt-oflo">Paracimatal</td>
                                        <td class="txt-oflo">0.9</td>
                                        <td><span class="text-success">25</span></td>
                                    </tr>
                                    <tr>
                                        <td>1
                                        </td>
                                        <td class="txt-oflo">Paracimatal</td>
                                        <td class="txt-oflo">2</td>
                                        <td><span class="text-success">25</span></td>
                                    </tr>
                                    <tr>
                                       <td>1
                                        </td>
                                        <td class="txt-oflo">Paracimatal</td>
                                        <td class="txt-oflo">0.5</td>
                                        <td><span class="text-success">25</span></td>
                                    </tr>
                                    <tr>
                                       <td>1
                                        </td>
                                        <td class="txt-oflo">Paracimatal</td>
                                        <td class="txt-oflo">2</td>
                                        <td><span class="text-success">25</span></td>
                                    </tr>
                                    <tr>
                                       <td>1
                                        </td>
                                        <td class="txt-oflo">Paracimatal</td>
                                        <td class="txt-oflo">2</td>
                                        <td><span class="text-success">25</span></td>
                                    </tr>
                                    <tr>
                                        <td>1
                                        </td>
                                        <td class="txt-oflo">Paracimatal</td>
                                        <td class="txt-oflo">2</td>
                                        <td><span class="text-success">25</span></td>
                                    </tr>
                                    <tr>
                                        <td>1
                                        </td>
                                        <td class="txt-oflo">Paracimatal</td>
                                        <td class="txt-oflo">2</td>
                                        <td><span class="text-success">25254</span></td>
                                    </tr>
                                </tbody>
                            </table>
                                </div>
                            </div>
                        </div>                        
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <p class="text-right mb-0"><b>Sub-total:</b> 2930.00</p>
                                <p class="text-right mb-0">HNIS: 0.9%</p>
                                <p class="text-right mb-0">VAT: 12.9%</p>
                                <hr>
                                <p class="text-right mb-0"><b>Total:</b>¢ 2930.00</p>
                            </div>                                    
                           
                        </div>
    </div><!-- ./wrapper -->

    <!-- AdminLTE App -->
   
  </body>