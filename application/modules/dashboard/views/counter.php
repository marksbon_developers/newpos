
<section class="content profile-page">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Counter</h2>
            <small class="text-muted">Inventory Portal</small>
        </div>        
        <div class="row clearfix">
           
            <div class="col-lg-8 col-md-8 col-sm-6">
                <div class="card">
                    <div class="body"> 
                        <div class="col-lg-12 col-md-8 col-sm-6">
                                <div class="form-group drop-custum">
                                    <select class="select2 form-control show-tick" onchange="fetch_itms(this.value)">
                                        <option value="">-- Search for Drugs --</option>
                                        <?php
                                      if(!empty($allproduct))
                                      {
                                        foreach($allproduct As $product) :
                                          print "<option value='".base64_encode($product->ProductCode)."'>$product->Item_Name - $                       product->Description</option>";
                                        endforeach;
                                      }
                                    ?>
                                    </select>
                                </div>
                            </div>

                            <hr>
                        <div class="row" id="ProductDisplay">
                            <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="info-box-3 bg-deep-orange">
                            <div class="icon"> 
                            <div class="text">Syproden Tab 500g</div>
                            <div class="text">(Depressants)</div>
                            </div>
                            <div class="content">
                            <div class="number">¢125.00</div>

                            </div>
                            </div>
                            </div>
                             <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="info-box-3 bg-orange">
                            <div class="icon"> 
                            <div class="text">Syproden Tab 500g</div>
                            <div class="text">(Stimulants.)</div>
                            </div>
                            <div class="content">
                            <div class="number">¢125.00</div>

                            </div>
                            </div>
                            </div>
                             <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="info-box-3 bg-red">
                            <div class="icon"> 
                            <div class="text">Syproden Tab 500g</div>
                            <div class="text">(Hallucinogens)</div>
                            </div>
                            <div class="content">
                            <div class="number">¢125.00</div>

                            </div>
                            </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="info-box-3 bg-light-blue">
                            <div class="icon"> 
                            <div class="text">Syproden Tab 500g</div>
                            <div class="text">(Opioids)</div>
                            </div>
                            <div class="content">
                            <div class="number">¢125.00</div>

                            </div>
                            </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="info-box-3 bg-lime">
                            <div class="icon"> 
                            <div class="text">Syproden Tab 500g</div>
                            <div class="text">(Inhalants)</div>
                            </div>
                            <div class="content">
                            <div class="number">¢125.00</div>

                            </div>
                            </div>
                            </div>
                             <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="info-box-3 bg-green">
                            <div class="icon"> 
                            <div class="text">Syproden Tab 500g</div>
                            <div class="text">(Cannabis)</div>
                            </div>
                            <div class="content">
                            <div class="number">¢125.00</div>

                            </div>
                            </div>
                            </div>
                             <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="info-box-3 bg-teal">
                            <div class="icon"> 
                            <div class="text">Syproden Tab 500g</div>
                            <div class="text">(psychoactive)</div>
                            </div>
                            <div class="content">
                            <div class="number">¢125.00</div>

                            </div>
                            </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="info-box-3 bg-brown">
                            <div class="icon"> 
                            <div class="text">Syproden Tab 500g</div>
                            <div class="text">(General)</div>
                            </div>
                            <div class="content">
                            <div class="number">¢125.00</div>

                            </div>
                            </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
             <div class="col-lg-4 col-md-4 col-sm-6">
                
                <div class="card">
                    <div class="header">
                        <div class="row sales-report jsdemo-notification-button">
                                <div class="col-lg-5 col-md-6 col-sm-6">
                                    <h2>Tally Board</h2>
                                    <br>
                                    <button type="button" class="btn bg-cyan waves-effect" data-toggle="modal" data-target="#confirm" data-color-name="bg-deep-purple">Sell</button>
                                </div>
                                <div class="col-lg-7 col-md-12 col-sm-12">
                                    <h1 class="text-right text-success m-t-20">¢4,231</h1>

                                </div>
                            </div>
                    </div>
                    <div class="body">
                        <div class="table-responsive" style="">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th style="width: 10%"><i class="zmdi zmdi-delete"></i></th>
                                        <th style="width: 10%">Qt</th>
                                        <th style="width: 50%">Desc.</th>
                                        <th style="width: 10%">Unit</th>
                                        <th style="width: 10%">Price</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="width: 10%"><i class="zmdi zmdi-delete"></i></td>
                                        <td>
                                        <select class="">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        </select>
                                        </td>
                                        <td class="txt-oflo">Paracimatal</td>
                                        <td class="txt-oflo">0.9</td>
                                        <td><span class="text-success">25</span></td>
                                        
                                    </tr>
                                    <tr>
                                        <td style="width: 10%"><i class="zmdi zmdi-delete"></i></td>
                                        <td>
                                        <select class="">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        </select>
                                        </td>
                                        <td class="txt-oflo">cyprodin</td>
                                        <td class="txt-oflo">0.9</td>
                                        <td><span class="text-success">25</span></td>
                                        
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Small Size -->
<div class="modal fade" id="confirm" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="smallModalLabel"><strong>who is selling ?</strong></h4>
            </div>
            <div class="modal-body"> 
               <div class="body">                       
                        <div class="row">

                            <div class="col-md-12">
                                <p class="text-right mb-0"><b>Sub-total:</b> 2930.00</p>
                                <p class="text-right mb-0">HNIS: 0.9%</p>
                                <p class="text-right mb-0">VAT: 12.9%</p>
                                <p class="text-right mb-0"><b>Total:</b><span class="badge bg-orange">¢ 2930.00</span></p>
                                   <input type="number" mini="0" class="form-control" placeholder="Customer contact Number">                             
                           <hr>
                                    <select class=" col-md-12  form-control show-tick">
                                        <option value="">All Users</option>
                                        <option value="10">Osborn</option>
                                        <option value="10">Bismark</option>
                                    </select>
                                </div>
                        </div>                            
                    </div>
                 </div>
            <div class="modal-footer">
                 <div class="col-md-12 text-right hidden-print ">
                                <hr>
                                <a href="javascript:void(0);" class="btn btn-raised bg-orange jsdemo-notification-button" data-dismiss="modal"  data-color-name="bg-deep-purple"  >Save</a>
                                 <a href="<?=base_url()?>dashboard/printout" class="btn btn-raised btn-success"><i class="zmdi zmdi-print" data-toggle="modal" data-target="#customersrecipt"></i>Save & Print</a>
                               
                            </div>
            </div>
        </div>
    </div>
</div>
<!-- Small Size 
<div class="modal fade" id="customersrecipt" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="smallModalLabel"><strong>marksbon pharmacy</strong></h4>
            </div>
            <div class="modal-body"> 
               <div class="body">
                        <div class="row">
                            <div class="col-12">
                                <div class="media">
                                    <img class="align-self-start mr-3" src="<?=base_url()?>resources/assets/images/logo-placeholder.jpg" width="70" alt="Swift">
                                    <div class="media-body">
                                        <h6>Invoice #:54667546<br>
                                        </h6>
                                    </div>
                                </div>
                                <hr>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <address>
                                    <strong>Twitter, Inc.</strong><br>
                                    795 Folsom Ave, Suite 546<br>
                                   (123) 456-34636
                                </address>
                            </div>
                            <div class="col-md-6 col-sm-6 text-right">
                                <p><strong>Order Date: </strong> Jun 15, 2016</p>
                                <p class="mb-0"><strong>Payment : </strong> <span class="badge bg-orange">Cash</span></p>
                            </div>
                        </div>                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Qt</th>
                                        <th>Desc.</th>
                                        <th>Unit</th>
                                        <th>Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1
                                        </td>
                                        <td class="txt-oflo">Paracimatal</td>
                                        <td class="txt-oflo">0.9</td>
                                        <td><span class="text-success">25</span></td>
                                    </tr>
                                    <tr>
                                        <td>1
                                        </td>
                                        <td class="txt-oflo">Paracimatal</td>
                                        <td class="txt-oflo">2</td>
                                        <td><span class="text-success">25</span></td>
                                    </tr>
                                    <tr>
                                       <td>1
                                        </td>
                                        <td class="txt-oflo">Paracimatal</td>
                                        <td class="txt-oflo">0.5</td>
                                        <td><span class="text-success">25</span></td>
                                    </tr>
                                    <tr>
                                       <td>1
                                        </td>
                                        <td class="txt-oflo">Paracimatal</td>
                                        <td class="txt-oflo">2</td>
                                        <td><span class="text-success">25</span></td>
                                    </tr>
                                    <tr>
                                       <td>1
                                        </td>
                                        <td class="txt-oflo">Paracimatal</td>
                                        <td class="txt-oflo">2</td>
                                        <td><span class="text-success">25</span></td>
                                    </tr>
                                    <tr>
                                        <td>1
                                        </td>
                                        <td class="txt-oflo">Paracimatal</td>
                                        <td class="txt-oflo">2</td>
                                        <td><span class="text-success">25</span></td>
                                    </tr>
                                    <tr>
                                        <td>1
                                        </td>
                                        <td class="txt-oflo">Paracimatal</td>
                                        <td class="txt-oflo">2</td>
                                        <td><span class="text-success">25254</span></td>
                                    </tr>
                                </tbody>
                            </table>
                                </div>
                            </div>
                        </div>                        
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <p class="text-right mb-0"><b>Sub-total:</b> 2930.00</p>
                                <p class="text-right mb-0">HNIS: 0.9%</p>
                                <p class="text-right mb-0">VAT: 12.9%</p>
                                <hr>
                                <p class="text-right mb-0"><b>Total:</b>¢ 2930.00</p>
                            </div>                                    
                           
                        </div>                            
                    </div>.
                 </div>
            <div class="modal-footer">
                 <div class="col-md-12 text-right hidden-print ">
                                <hr>
                               
                            </div>
            </div>
        </div>
    </div>
</div> -->