<section class="content home">
    <div class="container-fluid">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#sales">
                    <i class="zmdi zmdi-file-text"></i>
                    <span>All Stocks</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#newstock">
                    <i class="zmdi zmdi zmdi-case-download"></i>
                    <span>New Stock</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#category">
                    <i class="zmdi zmdi-file-text"></i>
                    <span>Category</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#suppliers">
                    <i class="zmdi zmdi-file-text"></i>
                    <span>Suppliers</span>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active in page-calendar" id="sales">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="header">
                                <ul class="header-dropdown">
                                    <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="zmdi zmdi-more-vert"></i></a>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="javascript:void(0);">Action</a></li>
                                            <li><a href="javascript:void(0);">Another action</a></li>
                                            <li><a href="javascript:void(0);">Something else here</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="body table-responsive">
                                <table class="table table-bordered table-striped table-hover" id="productDetails">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Supplier</th>
                                        <th>Unit Price</th>
                                        <th>Re-Order Level</th>
                                        <th>Exipry Date</th>
                                        <th>status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <!--<tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Supplier</th>
                                        <th>Exipry Date</th>
                                        <th>Unit Price</th>
                                        <th>Re-Order Level</th>
                                        <th>status</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>-->
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane " id="newstock">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="header">
                                <h2> New Product
                                    <?php
                                        if($this->session->flashdata('error'))
                                            print '<small class="display-block" style="color:red">'.@$this->session->flashdata('error').'</small>';
                                        else
                                            print '<small class="display-block">'.@$this->session->flashdata('success').'</small>';
                                    ?>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="zmdi zmdi-more-vert"></i></a>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                            <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>
                                            <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <form action="<?=base_url()?>products/store" method="post" >
                                    <div class="row clearfix jsdemo-notification-button">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <span style="color:red;">*</span>
                                                    <input type="text" class="form-control" placeholder="Item Name" name="name">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group drop-custum">
                                                <span style="color:red;">*</span>
                                                <select class="form-control show-tick" name="category">
                                                    <option label="Select Category" value=""></option>
                                                    <?php if(allCategories) : foreach ($allCategories as $category) : ?>
                                                        <option value="<?=$category->id?>"><?=$category->name?></option>
                                                    <?php endforeach; endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <span style="color:red;">*</span>
                                                <div class="form-line">
                                                    <select class="form-control show-tick" name="supplier">
                                                        <option label="Select Supplier" value=""></option>
																		  <?php if($allSuppliers) : foreach ($allSuppliers as $allSuppliers) : ?>
                                                            <option value="<?=$allSuppliers->id?>"><?=$allSuppliers->name?></option>
																		  <?php endforeach; endif; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <span style="color:red;">*</span>
                                                <div class="form-line">
                                                    <input type="number" class="datepicker form-control" placeholder="Unit Price" name="unit_price" min="0">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <span style="color:red;">*</span>
                                                <div class="form-line">
                                                    <input type="number" class="datepicker form-control" placeholder="Unit Quantity" name="unit_qty" min="0">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <span style="color:red;">*</span>
                                                <div class="form-line">
                                                    <input type="number" class="datepicker form-control" placeholder="Re-Order Level" min="0" name="reorder_level">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <span style="color:red;">*</span>
                                                <div class="form-line">
                                                    <input type="date" class="datepicker form-control" placeholder="Expiry Date" name="expiry_date">
                                                </div>
                                            </div>
                                        </div>
                                        <!--<div class="col-sm-3">
                                            <div class="form-group">
                                                <span style="color:red;">*</span>
                                                <div class="form-line">
                                                    <input type="number" class="form-control" placeholder="Surpplier's Contact">
                                                </div>
                                            </div>
                                        </div>-->
                                        <div class="col-sm-12">
                                            <div class="row clearfix jsdemo-notification-button">
                                            <button type="submit" class="btn btn-raised g-bg-cyan" name="newProductSubmit">Submit</button>
                                            <button type="button" class="btn btn-raised">Cancel</button>
                                        </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane page-calendar" id="category">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="header">
                                <h2>All Registered Stock</h2>
                                <ul class="header-dropdown">
                                    <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="zmdi zmdi-more-vert"></i></a>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="javascript:void(0);">Action</a></li>
                                            <li><a href="javascript:void(0);">Another action</a></li>
                                            <li><a href="javascript:void(0);">Something else here</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="body table-responsive">
                                <div class="row">
                                <div class="col-md-4">
                                    <form action="<?=base_url()?>category/store" method="post" >
                                        <div class="form-group">
                                            <div class="form-line">
                                                <span style="color:red;">*</span>
                                                <input type="text" class="form-control" placeholder="Item Name" name="name">
                                            </div>
                                        </div>
                                        <div class="row clearfix jsdemo-notification-button">
                                            <button type="submit" class="btn btn-raised g-bg-cyan" data-color-name="bg-black" name="newCategorySubmit">Submit</button>
                                            <button type="submit" class="btn btn-raised">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-8">
                                    <table id="newCategory" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane page-calendar" id="suppliers">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="header">
                                <h2>All Suppliers</h2>
                                <ul class="header-dropdown">
                                    <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="zmdi zmdi-more-vert"></i></a>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="javascript:void(0);">Action</a></li>
                                            <li><a href="javascript:void(0);">Another action</a></li>
                                            <li><a href="javascript:void(0);">Something else here</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="body table-responsive">
                                <div class="row">
                                    <div class="col-md-4">
                                        <form action="<?=base_url()?>supplier/store" method="post" >
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <span style="color:red;">*</span>
                                                    <input type="text" class="form-control" placeholder="Enter Supplier Name" name="name">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <span style="color:red;">*</span>
                                                    <input type="text" class="form-control" placeholder="Supplier Contact" name="contact">
                                                </div>
                                            </div>
                                            <div class="row clearfix jsdemo-notification-button">
                                                <button type="submit" class="btn btn-raised g-bg-cyan" data-color-name="bg-black" name="newSupplierSubmit">Submit</button>
                                                <button type="submit" class="btn btn-raised">Cancel</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-8">
                                        <table id="newSupplier" class="table table-bordered table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <!--<th>ID</th>-->
                                                <th>Name</th>
                                                <th>Contact</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>