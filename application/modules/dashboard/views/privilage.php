
<section class="content profile-page">
    <div class="container-fluid">
        <div class="block-header">
            <h2>User Previllages</h2>
            <small class="text-muted">Assignments</small>
        </div>        
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2>Previllages Panel</h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="zmdi zmdi-more-vert"></i></a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="row sales-report">
                            </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <span style="color:red;">*</span>
                                    <select class="form-control show-tick">
                                        <option value="">All Users</option>
                                        <option value="10">Osborn</option>
                                        <option value="10">Bismark</option>
                                    </select>
                                </div>
                               <div class="col-sm-12">
                                <div class="row clearfix jsdemo-notification-button">
                                <button type="submit" class="btn btn-raised g-bg-cyan"data-color-name="bg-black">Submit</button>
                                <button type="submit" class="btn btn-raised">Cancel</button> 
                            </div>
                            </div>
                            </div>
                            <div class="col-md-6">
                                <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="checkbox">
                                <input type="checkbox" id="basic_checkbox_2" />
                                <label for="basic_checkbox_2"></label>
                                </div>
                                        </td>
                                        <td>
                                            Dashboard
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="checkbox">
                                <input type="checkbox" id="basic_checkbox_3" />
                                <label for="basic_checkbox_3"></label>
                                </div>
                                        </td>
                                        <td>
                                        Counter
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="checkbox">
                                <input type="checkbox" id="basic_checkbox_4" />
                                <label for="basic_checkbox_4"></label>
                                </div>
                                        </td>
                                        <td>
                                            Daily Sales
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="checkbox">
                                <input type="checkbox" id="basic_checkbox_5" />
                                <label for="basic_checkbox_5"></label>
                                </div>
                                        </td>
                                        <td>
                                            New Stock
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="checkbox">
                                <input type="checkbox" id="basic_checkbox_6" />
                                <label for="basic_checkbox_6"></label>
                                </div>
                                        </td>
                                        <td>
                                            Edit Stock
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="checkbox">
                                <input type="checkbox" id="basic_checkbox_7" />
                                <label for="basic_checkbox_7"></label>
                                </div>
                                        </td>
                                        <td>
                                            Update Stock
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="checkbox">
                                <input type="checkbox" id="basic_checkbox_8" />
                                <label for="basic_checkbox_8"></label>
                                </div>
                                        </td>
                                        <td>
                                            Report
                                        </td>
                                    </tr>
                                     <tr>
                                        <td>
                                            <div class="checkbox">
                                <input type="checkbox" id="basic_checkbox_9" />
                                <label for="basic_checkbox_9"></label>
                                </div>
                                        </td>
                                        <td>
                                            Administrator
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>