
<section class="content profile-page">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Daily Sales</h2>
            <small class="text-muted">Cash At Hand</small>
        </div>        
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2>Recent sales</h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="zmdi zmdi-more-vert"></i></a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="row sales-report">
                                <div class="col-lg-6 col-md-6 col-sm-12 jsdemo-notification-button">
                                    <h2>
                                        <select class="form-control show-tick">
                                        <option value="">Store Branch</option>
                                        <option value="10">General</option>
                                        <option value="20">Depressants</option>
                                         <option value="20">Stimulants</option>
                                          <option value="20">Hallucinogens</option>
                                           <option value="20">Opioids</option>
                                            <option value="20">Inhalants</option>
                                            <option value="20">Cannabis</option>
                                            <option value="20">psychoactive</option>
                                    </select>
                                    </h2>
                                    <h2>
                                        <div class="row">
                                            <div class="col-md-6">
                                              <input type="date" class="form-control" placeholder="Date Name">  
                                            </div>
                                            <div class="col-md-6">
                                              <input type="date" class="form-control" placeholder="Date Name">  
                                            </div>
                                            
                                        </div>
                                        
                                    <p> <button type="submit" class="btn btn-raised g-bg-cyan  " data-color-name="bg-deep-purple">Generate</button></p>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <h1 class="text-right text-success m-t-20">¢4,231</h1>
                                </div>
                            </div>
                        <div class="body table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Description</th>
                                        <th>Quantity</th>
                                        <th>DATE</th>
                                        <th>Unit P.</th>
                                        <th>PRICE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td class="txt-oflo">Ipsum is simply</td>
                                        <td>2 </td>
                                        <td class="txt-oflo">Feb 11, 2017</td>
                                        <td><span class="text-success">¢2</span></td>
                                        <td><span class="text-success">¢25</span></td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td class="txt-oflo">Lorem Ipsum is</td>
                                        <td>1</td>
                                        <td class="txt-oflo">March 29, 2017</td>
                                        <td><span class="text-info">$1234</span></td>
                                        <td><span class="text-info">$1234</span></td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td class="txt-oflo">Lorem Ipsum is simply</td>
                                        <td>5</td>
                                        <td class="txt-oflo">April 21, 2017</td>
                                        <td><span class="text-info">$1234</span></td>
                                        <td><span class="text-danger">-$204</span></td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td class="txt-oflo">Hosting press html</td>
                                        <td>4</td>
                                        <td class="txt-oflo">Jun 22, 2017</td>
                                          <td><span class="text-success">$24</span></td>
                                        <td><span class="text-success">$24</span></td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td class="txt-oflo">Lorem is simply</td>
                                        <td>6</td>
                                        <td class="txt-oflo">July 20, 2017</td>
                                          <td><span class="text-success">$24</span></td>
                                        <td><span class="text-success">$21</span></td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td class="txt-oflo">Lorem Ipsum simply</td>
                                        <td>2 </td>
                                        <td class="txt-oflo">July 21, 2017</td>
                                          <td><span class="text-success">$24</span></td>
                                        <td><span class="text-danger">-$12</span></td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td class="txt-oflo">Lorem Ipsum is simply</td>
                                        <td>1</td>
                                        <td class="txt-oflo">July 21, 2017</td>
                                          <td><span class="text-success">$24</span></td>
                                        <td><span class="text-success">$54</span></td>
                                    </tr>
                                </tbody>
                            </table>
                            
                            <a href="javascript:window.print()" class="btn btn-raised btn-success"><i class="zmdi zmdi-print"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>