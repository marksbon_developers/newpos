
<section class="content home">
    <div class="container-fluid">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item"><a class="nav-link " data-toggle="tab" href="#income"><i class="zmdi zmdi zmdi-case-download"></i> <span>Edit Stock </span></a></li>
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#sales"><i class="zmdi zmdi-file-text"></i> <span> Stock Manager</span></a></li>
        </ul> 
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane page-calendar" id="income">
                <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2>Register Information <small>Bio Data</small> </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="zmdi zmdi-more-vert"></i></a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <span style="color:red;">*</span>
                                        <input type="text" class="form-control" placeholder="Full Name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group drop-custum">
                                    <span style="color:red;">*</span>
                                    <select class="form-control show-tick">
                                        <option value="">Role </option>
                                        <option value="10">Operator</option>
                                        <option value="20">sales exercutive</option>
                                         <option value="20">Manager</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <span style="color:red;">*</span>
                                    <div class="form-line">
                                        <input type="date" class="datepicker form-control" placeholder="Username" data-dtp="dtp_UNcwJ">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <span style="color:red;">*</span>
                                    <div class="form-line">
                                        <input type="password" class="datepicker form-control" placeholder="Password" data-dtp="dtp_UNcwJ">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="password" mini="0" class="form-control" placeholder="Confirm Password">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="email" class="form-control" placeholder="Email">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <span style="color:red;">*</span>
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Address">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <span style="color:red;">*</span>
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Gurantor">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                 <div class="form-group">
                                    <span style="color:red;">*</span>
                                    <div class="form-line">
                                        <input type="number" class="form-control" placeholder="Contact">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="row clearfix jsdemo-notification-button">
                                <button type="submit" class="btn btn-raised g-bg-cyan" data-color-name="bg-black">Update</button>
                                <button type="submit" class="btn btn-raised">Cancel</button> 
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                                
            </div>
            
            <div role="tabpanel" class="tab-pane active in" id="sales">
                <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2>Stock Manager</h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="zmdi zmdi-more-vert"></i></a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body table-responsive">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Full Name</th>
                                    <th>Username</th>
                                    <th>Contact No.#</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Garantor</th>
                                    <th>Contact</th>                                
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                     <th>#</th>
                                    <th>Full Name</th>
                                    <th>Username</th>
                                    <th>Contact No.#</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Garantor</th>
                                    <th>Contact</th> 
                                    <th>Action</th>  
                                </tr>
                            </tfoot>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Bismark Offei</td>
                                    <td>BOFFEI</td>
                                    <td>02456264875</td>
                                    <td>wikills22@<gmail class="com"></gmail></td>
                                    <td>kumasi</td>
                                    <td>Samuel</td>
                                    <td>021456986</td>
                                    <td><i class="zmdi zmdi-file-text"></i></td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Bismark Offei</td>
                                    <td>BOFFEI</td>
                                    <td>02456264875</td>
                                    <td>wikills22@<gmail class="com"></gmail></td>
                                    <td>kumasi</td>
                                    <td>Samuel</td>
                                    <td>021456986</td>
                                    <td><i class="zmdi zmdi-file-text"></i></td>
                                </tr>
                                <tr>
                                 <td>1</td>
                                    <td>Bismark Offei</td>
                                    <td>BOFFEI</td>
                                    <td>02456264875</td>
                                    <td>wikills22@<gmail class="com"></gmail></td>
                                    <td>kumasi</td>
                                    <td>Samuel</td>
                                    <td>021456986</td>
                                    <td><i class="zmdi zmdi-file-text"></i></td>
                                </tr>
                                <tr>
                                   <td>1</td>
                                    <td>Bismark Offei</td>
                                    <td>BOFFEI</td>
                                    <td>02456264875</td>
                                    <td>wikills22@<gmail class="com"></gmail></td>
                                    <td>kumasi</td>
                                    <td>Samuel</td>
                                    <td>021456986</td>
                                    <td><i class="zmdi zmdi-file-text"></i></td>
                                </tr>
                               
                                <tr>
                                    <td>1</td>
                                    <td>Bismark Offei</td>
                                    <td>BOFFEI</td>
                                    <td>02456264875</td>
                                    <td>wikills22@gmail.vom</td>
                                    <td>kumasi</td>
                                    <td>Samuel</td>
                                    <td>021456986</td>
                                    <td><i class="zmdi zmdi-file-text"></i></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
            </div>            
        </div>
    </div>
</section>