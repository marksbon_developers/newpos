<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MX_Controller
{
    /*******************************
      Constructor 
    *******************************/
    public function __construct() {
      parent::__construct();
    }

	 /*******************************
		  Retrieve All
	  *******************************/
	 public function getall()
	 {
		  if(isset($_SESSION['user']['id'])) :
		  	 	# Loading Helper / Models
				$this->load->model('CategoryModel');
		  		$query_result = $this->CategoryModel->getAllCategory();

				print_r(json_encode($query_result));
		  else :
				print "";
		  endif;
	 }

    /*******************************
        Create New
    *******************************/
    public function store()
    {
        if(isset($_POST['newCategorySubmit'])) {
            $this->form_validation->set_rules('name', 'Category Name', 'required|trim|is_unique[product_category.name]');

            if ($this->form_validation->run() === FALSE) {
                $this->session->set_flashdata('error', validation_errors());
                redirect('dashboard/newstock');
            }
            else {
                # Loading Helper / Models
                $this->load->model('CategoryModel');

                $categoryName = $this->input->post('name');
                $response = $this->CategoryModel->newCategory($categoryName);

                if($response)
                    $this->session->set_flashdata('success', "Category Added Successful");
                else
                    $this->session->set_flashdata('error', "Category Add Failed");

                redirect('dashboard/newstock');
            }
        }
        else
            redirect('dashboard');
    }

	 /*******************************
	 	Update Info
	  ******************************/
	 public function update()
	 {
		  if(isset($_POST['updateCategorySubmit'])) {
				$this->form_validation->set_rules('category_id', 'Category ID', 'required|trim');
				$this->form_validation->set_rules('name', 'Category Name', 'required|trim|is_unique[product_category.name]');

				if ($this->form_validation->run() === FALSE) {
					 $this->session->set_flashdata('error', validation_errors());
					 redirect('dashboard/newstock');
				}
				else {
					 # Loading Helper / Models
					 $this->load->model('CategoryModel');

					 $updateData = [
					 	 'name' => $this->input->post('name'),
						  'updated_by' => $_SESSION['user']['id']
					 ];
					 $categoryID = $this->input->post('category_id');

					 $response = $this->CategoryModel->updateCategory($updateData,$categoryID);

					 if($response)
						  $this->session->set_flashdata('success', "Category Updated");
					 else
						  $this->session->set_flashdata('error', "Update Failed");

					 redirect('dashboard/newstock');
				}
		  }
		  else
				redirect('dashboard');
	 }

	 /*******************************
	 Edit Status
	  ******************************/
	 public function status()
	 {
		  if(isset($_POST['statusCategorySubmit'])) {
				$this->form_validation->set_rules('category_id', 'Category', 'required|trim');
				$this->form_validation->set_rules('status', 'Category Status', 'required|trim');

				if ($this->form_validation->run() === FALSE) {
					 $this->session->set_flashdata('error', validation_errors());
					 redirect('dashboard/newstock');
				}
				else {
					 # Loading Helper / Models
					 $this->load->model('CategoryModel');

					 $updateData = [
						  'status' => $this->input->post('status'),
						  'updated_by' => $_SESSION['user']['id']
					 ];
					 $categoryID = $this->input->post('category_id');

					 $response = $this->CategoryModel->updateCategory($updateData,$categoryID);

					 if($response)
						  $this->session->set_flashdata('success', "Category Status Updated");
					 else
						  $this->session->set_flashdata('error', "Update Failed");

					 redirect('dashboard/newstock');
				}
		  }
		  else
				redirect('dashboard');
	 }
}//End of Class
