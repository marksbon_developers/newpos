<div class="container">
    <div class="card-top"></div>
    <div class="card">
        <h1 class="title"><span><img class="align-self-start mr-3" src="<?=base_url()?>resources/assets/images/logobb.png" width="70" alt="www.marksbon.com"></span>Request <span class="msg">Register a new company</span></h1>
        <div class="body">
            <form>
                <div class="input-group icon before_span">
                    <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                    <div class="form-line">
                        <input type="text" class="form-control" name="namesurname" placeholder="Contact person Name" required="" autofocus>
                    </div>
                </div>
                <div class="input-group icon before_span">
                    <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                    <div class="form-line">
                        <input type="email" class="form-control" name="email" placeholder="Email Address" required="">
                    </div>
                </div>
                <div class="input-group icon before_span">
                    <span class="input-group-addon"><i class="zmdi zmdi-balance col-cyan"></i></span>
                    <div class="form-line">
                        <input type="text" class="form-control" name="password" minlength="6" placeholder="Company" required="">
                    </div>
                </div>
                <div class="input-group icon before_span">
                    <span class="input-group-addon"><i class="zmdi zmdi-lock"></i></span>
                    <div class="form-line">
                        <input type="number" class="form-control" name="confirm" minlength="6" placeholder="Contact number" required="">
                    </div>
                </div>
                <div>
                    <input type="checkbox" name="terms" id="terms" class="filled-in chk-col-pink">
                    <label for="terms">I read and agree to the <a href="javascript:void(0);">terms of usage</a>.</label>
                </div>
                <div class="text-center">
                    <a href="javascript:void(0);" class="btn btn-raised g-bg-cyan waves-effect">SIGN UP</a>
                </div>
                <div class="align-center">
                    <a href="<?=base_url()?>access/login">You already have a membership?</a>
                </div>
            </form>
        </div>
    </div>  
</div>
<div class="theme-bg"></div>