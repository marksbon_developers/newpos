<div class="container">
    <div class="card-top"></div>
    <div class="card locked">
        <h1 class="title"><span><img class="align-self-start mr-3" src="<?=base_url()?>resources/assets/images/logobb.png" width="70" alt="www.marksbon.com"></span>Locked</h1>
        <div class="d-flex">
            <div class="thumb">
                <img class="media-object" src="<?=base_url()?>resources/assets/images/random-avatar7.jpg" class="rounded" alt="">                
            </div>
            <div>
                <h6 class="media-heading">Dr. John Smith</h6>
                <p>Operator</p>
            </div>
        </div>
        <div class="body">
            <form>
                <div class="input-group icon before_span">
                    <span class="input-group-addon"> <i class="zmdi zmdi-lock"></i> </span>
                    <div class="form-line">
                        <input type="password" class="form-control" name="password" placeholder="Password" required>
                    </div>
                </div>
                <div class="text-center">
                    <a href="" class="btn btn-raised waves-effect g-bg-cyan">Login</a>
                </div>
                <div class="text-center"><a href="<?=base_url()?>access/login">Sign In!</a></div>
            </form>
        </div>
    </div>    
</div>

<div class="theme-bg"></div>