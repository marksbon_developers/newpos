<div class="container">
    <div class="card-top"></div>
    <div class="card">
        <h1 class="title" style="margin-bottom: 3px"> Login </h1>
        <h5 style="color: red; margin-left: 30px; ">
            <?php
            if($this->session->flashdata('error'))
                print '<small class="display-block" style="color:red; font-size: 13px;">'.@$this->session->flashdata('error').'</small>';
            elseif($this->session->flashdata('attemptleft') > 0)
                print '<small class="display-block" style="color:red">Invalid Login Credentials. <b>Login Attempt Left: '.@$this->session->flashdata('attemptleft').'</b></small>';
            else
                print '<small class="display-block">Authorized Personnels Only</small>';
            ?>

        </h5>
        <div class="body">
            <form action="<?=base_url()?>access/login_validation" method="post">
                <div class="input-group icon before_span">
                    <span class="input-group-addon"> <i class="zmdi zmdi-account"></i> </span>
                    <div class="form-line">
                        <input type="email" class="form-control" name="username" placeholder="Username" required autofocus>
                    </div>
                </div>
                <div class="input-group icon before_span">
                    <span class="input-group-addon"> <i class="zmdi zmdi-lock"></i> </span>
                    <div class="form-line">
                        <input type="password" class="form-control" name="password" placeholder="Password" required>
                    </div>
                </div>
                <div>
                    <div class="">
                        <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                        <label for="rememberme">Remember Me</label>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-raised waves-effect g-bg-cyan" name="login">SIGN IN</button>
                        <!--<a href="<?/*=base_url()*/?>access/request" class="btn btn-raised waves-effect">REQUEST </a>-->
                    </div>
                    <div class="text-center"> <a href="<?=base_url()?>access/forgotten">Forgot Password?</a></div>
                </div>
            </form>
        </div>
    </div>    
</div>

<div class="theme-bg"></div>