<div class="container">
    <div class="card-top"></div>
    <div class="card">
        <h1 class="title"><span><img class="align-self-start mr-3" src="<?=base_url()?>resources/assets/images/logobb.png" width="70" alt="www.marksbon.com"></span>Forgot Password? <div class="msg">Enter your e-mail address below to reset your password.</div></h1>
        <div class="body">
            <form>
                <div class="input-group icon before_span">
                    <span class="input-group-addon"> <i class="zmdi zmdi-email"></i> </span>
                    <div class="form-line">
                        <input type="email" class="form-control" name="email" placeholder="Email" required autofocus>
                    </div>
                </div>              
                <div class="row">                    
                    <div class="col-sm-12 text-center">
                        <a href="" class="btn btn-raised waves-effect g-bg-cyan">RESET MY PASSWORD</a>
                    </div>
                    <div class="col-sm-12 text-center"> <a href="<?=base_url()?>access/login">Sign In!</a> </div>
                </div>
            </form>
        </div>
    </div>    
</div>
<div class="theme-bg"></div>