    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?=base_url()?>resources/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?=base_url()?>resources/assets/node_modules/popper/popper.min.js"></script>
    <script src="<?=base_url()?>resources/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!--Custom JavaScript -->
    <script type="text/javascript">
        $(function() {
            $(".preloader").fadeOut();
        });
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
        // ============================================================== 
        // Login and Recover Password 
        // ============================================================== 
        $('#to-recover').on("click", function() {
            $("#loginform").slideUp();
            $("#recoverform").fadeIn();
        });
    </script>
    
</body>


<!-- Mirrored from eliteadmin.themedesigner.in/demos/bt4/hospital/pages-login-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 19 Oct 2018 10:41:15 GMT -->
</html>