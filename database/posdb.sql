-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for pos_db
CREATE DATABASE IF NOT EXISTS `pos_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `pos_db`;

-- Dumping structure for table pos_db.access_login_failed
CREATE TABLE IF NOT EXISTS `access_login_failed` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT 'auto',
  `user_id` int(10) NOT NULL,
  `username` varchar(255) NOT NULL COMMENT 'the username used for logging in ',
  `password` varchar(255) NOT NULL COMMENT 'encrypted password used for logging in ',
  `user_agent` varchar(255) NOT NULL COMMENT 'contains browser info used to access the system',
  `hostname` varchar(255) NOT NULL COMMENT 'computer name used to access the system',
  `ipaddress` varchar(20) NOT NULL DEFAULT '0.0.0.0' COMMENT 'ip address of the computer',
  `city_region` text COMMENT 'location of the user when accessing the system',
  `country` varchar(255) DEFAULT NULL COMMENT 'country from which the system was accessed',
  `access_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'date of access',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.access_login_failed: 0 rows
/*!40000 ALTER TABLE `access_login_failed` DISABLE KEYS */;
INSERT INTO `access_login_failed` (`id`, `user_id`, `username`, `password`, `user_agent`, `hostname`, `ipaddress`, `city_region`, `country`, `access_date`) VALUES
	(1, 1, 'marksbonapps@gmail.com', 'e807f1fcf82d132f9bb018ca6738a19f', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 'DESKTOP-KJO4PF0', '127.0.0.1', NULL, NULL, '2019-08-27 13:15:55'),
	(2, 1, 'marksbonapps@gmail.com', 'cc0bd5832b4e1465a6987d953bb767af', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 'DESKTOP-KJO4PF0', '127.0.0.1', NULL, NULL, '2019-08-27 13:16:02'),
	(3, 1, 'marksbonapps@gmail.com', '992a6d18b2a148cf20d9014c3524aa11', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 'DESKTOP-KJO4PF0', '127.0.0.1', NULL, NULL, '2019-08-27 13:17:10'),
	(4, 1, 'marksbonapps@gmail.com', '96fef212944b86ce7edd8eeac876bc23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 'DESKTOP-KJO4PF0', '127.0.0.1', NULL, NULL, '2019-08-27 13:23:08');
/*!40000 ALTER TABLE `access_login_failed` ENABLE KEYS */;

-- Dumping structure for table pos_db.access_login_successful
CREATE TABLE IF NOT EXISTS `access_login_successful` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT 'auto generated id',
  `user_id` int(5) NOT NULL COMMENT 'auto generated id from users table',
  `time_in` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'time of login by user',
  `time_out` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'time of logout by user',
  `online` int(1) NOT NULL DEFAULT '1' COMMENT 'online status of the user',
  `user_agent` varchar(255) NOT NULL COMMENT 'browser info of the user at the time of system access',
  `ipaddress` varchar(20) NOT NULL DEFAULT '0.0.0.0' COMMENT 'ip address of the user at the time of system access',
  `hostname` varchar(255) NOT NULL COMMENT 'computer name of the user at the time of system access',
  `city_region` text COMMENT 'city & region of the user at the time of system access',
  `country` varchar(255) DEFAULT NULL COMMENT 'country of the user at the time of system access',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.access_login_successful: 1 rows
/*!40000 ALTER TABLE `access_login_successful` DISABLE KEYS */;
INSERT INTO `access_login_successful` (`id`, `user_id`, `time_in`, `time_out`, `online`, `user_agent`, `ipaddress`, `hostname`, `city_region`, `country`) VALUES
	(1, 1, '2019-08-27 02:39:22', '0000-00-00 00:00:00', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', '127.0.0.1', 'DESKTOP-KJO4PF0', NULL, NULL),
	(2, 1, '2019-08-27 13:17:00', '0000-00-00 00:00:00', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', '127.0.0.1', 'DESKTOP-KJO4PF0', NULL, NULL),
	(3, 1, '2019-08-27 13:23:14', '0000-00-00 00:00:00', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', '127.0.0.1', 'DESKTOP-KJO4PF0', NULL, NULL),
	(4, 1, '2019-08-27 14:01:04', '0000-00-00 00:00:00', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Insomnia/6.6.2 Chrome/66.0.3359.181 Electron/3.1.13 Safari/537.36', '127.0.0.1', 'DESKTOP-KJO4PF0', NULL, NULL);
/*!40000 ALTER TABLE `access_login_successful` ENABLE KEYS */;

-- Dumping structure for table pos_db.access_roles_privileges_group
CREATE TABLE IF NOT EXISTS `access_roles_privileges_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'name of the group / predefined set of roles & privileges ',
  `roles` text NOT NULL COMMENT 'A set of predefined roles to set a user ',
  `privileges` text NOT NULL COMMENT 'A set of predefined privileges from roles',
  `description` varchar(255) DEFAULT NULL COMMENT 'A summary of text describing that position',
  `login_url` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active' COMMENT 'State of the group whether its active(1) or inactive(0)',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table pos_db.access_roles_privileges_group: 5 rows
/*!40000 ALTER TABLE `access_roles_privileges_group` DISABLE KEYS */;
INSERT INTO `access_roles_privileges_group` (`id`, `name`, `roles`, `privileges`, `description`, `login_url`, `status`, `date_created`) VALUES
	(1, 'System Developers', 'New Request|History|Directory|Report|Task|Assign-Ticket-Task|Users|Privileges|Assigned-Tickets', '', 'Designers of this software', 'dashboard', 'active', '2017-10-16 22:42:32'),
	(2, 'End Users', 'New Request|History|Directory', '', 'Chief Executive Officer Of The Whole Business', '', 'active', '2019-01-23 05:54:44'),
	(3, 'Assignees', 'New Request|History|Directory|Controls|Assigned-Tickets', '', 'Recieves Clients Warmly And Also The First Point Of Contact. Front Desk Things Too', '', 'active', '2019-01-23 05:56:09'),
	(4, 'Manager', 'New Request|History|Directory|Controls|Report|Task|Assigned|Assign-Ticket-Task', '', NULL, '', 'active', '2019-05-07 11:55:53'),
	(5, 'Administrator', 'New Request|History|Directory|Report|Task|Assign-Ticket-Task|Users|Privileges|Assigned-Tickets', '', NULL, '', 'active', '2019-05-07 11:55:53');
/*!40000 ALTER TABLE `access_roles_privileges_group` ENABLE KEYS */;

-- Dumping structure for table pos_db.access_roles_privileges_user
CREATE TABLE IF NOT EXISTS `access_roles_privileges_user` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL COMMENT 'Contains the id of the user',
  `custom_roles` text CHARACTER SET utf8 NOT NULL COMMENT 'Contains the Exceptional/Added Roles of a user',
  `custom_privileges` text CHARACTER SET utf8 NOT NULL COMMENT 'Contains the Permissible Actions on the Exceptional Roles assigned to the user',
  `group_id` int(11) NOT NULL COMMENT 'Contains the id of the group to which the user belongs to in roles & priviledges',
  `status` enum('active','inactive','deleted','') CHARACTER SET utf8 DEFAULT 'active' COMMENT 'state of the user''s permissions.',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table pos_db.access_roles_privileges_user: 8 rows
/*!40000 ALTER TABLE `access_roles_privileges_user` DISABLE KEYS */;
INSERT INTO `access_roles_privileges_user` (`id`, `user_id`, `custom_roles`, `custom_privileges`, `group_id`, `status`) VALUES
	(1, 1, '', '', 1, 'active'),
	(2, 2, '', '', 2, 'active'),
	(3, 11, '', '', 3, 'active'),
	(4, 12, '', '', 4, 'active'),
	(5, 13, '', '', 5, 'active'),
	(6, 14, '', '', 4, 'active'),
	(7, 15, '', '', 3, 'active'),
	(8, 16, '', '', 2, 'active');
/*!40000 ALTER TABLE `access_roles_privileges_user` ENABLE KEYS */;

-- Dumping structure for table pos_db.access_users
CREATE TABLE IF NOT EXISTS `access_users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 NOT NULL,
  `passwd` varchar(100) CHARACTER SET utf8 NOT NULL,
  `fullname` varchar(255) CHARACTER SET utf8 NOT NULL,
  `phone_number` varchar(20) CHARACTER SET utf8 NOT NULL,
  `biodata_id` bigint(20) NOT NULL DEFAULT '0',
  `login_attempt` tinyint(1) NOT NULL DEFAULT '5',
  `online` tinyint(4) NOT NULL,
  `status` enum('active','inactive','deleted','') CHARACTER SET utf8 NOT NULL DEFAULT 'active',
  `created_by` bigint(20) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table pos_db.access_users: ~1 rows (approximately)
/*!40000 ALTER TABLE `access_users` DISABLE KEYS */;
INSERT INTO `access_users` (`id`, `username`, `passwd`, `fullname`, `phone_number`, `biodata_id`, `login_attempt`, `online`, `status`, `created_by`, `date_created`) VALUES
	(1, 'marksbonapps@gmail.com', '$2y$10$O/swrfsJ6TbhtHiyzR7GmurgN4u49VcaMFzrOtZ9.3N511hoPhOVi', 'Osborne Mordreds', '0541786220', 0, 5, 0, 'active', 0, '2018-07-03 15:59:37');
/*!40000 ALTER TABLE `access_users` ENABLE KEYS */;

-- Dumping structure for table pos_db.company_info
CREATE TABLE IF NOT EXISTS `company_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `telephone_1` varchar(20) NOT NULL,
  `telephone_2` varchar(20) NOT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `postal_address` varchar(255) NOT NULL,
  `residence_address` varchar(255) NOT NULL COMMENT 'physical location and street name where company is located',
  `website` varchar(100) DEFAULT NULL COMMENT 'website of the comany',
  `mission` varchar(255) DEFAULT NULL,
  `vision` varchar(255) DEFAULT NULL,
  `gps_location` varchar(255) DEFAULT NULL,
  `tin_number` varchar(50) DEFAULT NULL,
  `logo_path` varchar(50) NOT NULL,
  `date_of_commence` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table pos_db.company_info: 1 rows
/*!40000 ALTER TABLE `company_info` DISABLE KEYS */;
INSERT INTO `company_info` (`id`, `name`, `telephone_1`, `telephone_2`, `fax`, `email`, `postal_address`, `residence_address`, `website`, `mission`, `vision`, `gps_location`, `tin_number`, `logo_path`, `date_of_commence`) VALUES
	(1, 'Okomfo Anokye Rural Bank', '+233 0204344906', '0246543708', NULL, 'Info@okomfoanokyeruralbank.com', 'Okomfo Anokye Rural Bank Ltd\r\nPostal Office Box 13\r\nWiamoase Ashanti\r\nKumasi', 'Kumasi-Wiamoase', 'www.okomfoanokyeruralbank.com', 'MISSION STATEMENT', 'VISION STATEMAN', 'GA-125-69631', 'TN-12345678', 'resources/img/logo.png', '0000-00-00');
/*!40000 ALTER TABLE `company_info` ENABLE KEYS */;

-- Dumping structure for table pos_db.product_audit
CREATE TABLE IF NOT EXISTS `product_audit` (
  `invoice_no` varchar(50) NOT NULL,
  `total_cost` int(11) NOT NULL,
  `sup_id` varchar(20) NOT NULL,
  `vendor` varchar(50) NOT NULL,
  `date_recv` varchar(50) NOT NULL,
  `comb_prod_id` text NOT NULL,
  `comb_unit_price` text NOT NULL,
  `comb_qty` text NOT NULL,
  `employee_id` varchar(20) NOT NULL,
  `comb_cost_p` varchar(255) NOT NULL,
  `comb_desc_id` varchar(255) NOT NULL,
  `comb_unit` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.product_audit: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_audit` ENABLE KEYS */;

-- Dumping structure for table pos_db.product_category
CREATE TABLE IF NOT EXISTS `product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'active',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cat_name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.product_category: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_category` ENABLE KEYS */;

-- Dumping structure for table pos_db.product_customers
CREATE TABLE IF NOT EXISTS `product_customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(50) NOT NULL,
  `mobile_number_1` varchar(20) NOT NULL,
  `mobile_number_2` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive','deleted','') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.product_customers: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_customers` DISABLE KEYS */;
INSERT INTO `product_customers` (`id`, `fullname`, `mobile_number_1`, `mobile_number_2`, `address`, `email`, `location`, `date_created`, `status`) VALUES
	(2, 'Kwaku', '65748392', '54673829', 'Tyrueiw', 'Df@yta', 'Gehjw', '2017-05-09 00:44:40', 'active');
/*!40000 ALTER TABLE `product_customers` ENABLE KEYS */;

-- Dumping structure for table pos_db.product_details
CREATE TABLE IF NOT EXISTS `product_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `category_id` int(20) NOT NULL,
  `unit_qty` int(5) NOT NULL,
  `cost_price` float DEFAULT NULL,
  `unit_price` float NOT NULL,
  `avail_qty` bigint(20) DEFAULT NULL,
  `expiry` varchar(20) DEFAULT NULL,
  `promo_qty` int(5) DEFAULT NULL,
  `promo_price` float DEFAULT NULL,
  `img_path` varchar(100) DEFAULT NULL,
  `status` enum('active','suspended','deleted') DEFAULT 'active',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.product_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_details` ENABLE KEYS */;

-- Dumping structure for table pos_db.product_suppliers
CREATE TABLE IF NOT EXISTS `product_suppliers` (
  `sup_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `tel1` varchar(20) NOT NULL,
  `tel2` varchar(20) DEFAULT NULL,
  `addr` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `loc` varchar(50) NOT NULL,
  `pay_type` varchar(20) DEFAULT NULL,
  `prod_type` text NOT NULL,
  PRIMARY KEY (`sup_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.product_suppliers: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_suppliers` DISABLE KEYS */;
INSERT INTO `product_suppliers` (`sup_id`, `name`, `tel1`, `tel2`, `addr`, `email`, `loc`, `pay_type`, `prod_type`) VALUES
	(1, 'vender', '0000000000', '0000000000', 'mall', 'mall@fotostore.com', 'mall', 'cash', '5x5.8 inch');
/*!40000 ALTER TABLE `product_suppliers` ENABLE KEYS */;

-- Dumping structure for table pos_db.product_suppliers_accounts
CREATE TABLE IF NOT EXISTS `product_suppliers_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sup_id` int(11) NOT NULL,
  `bank` varchar(50) NOT NULL,
  `branch` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `num` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.product_suppliers_accounts: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_suppliers_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_suppliers_accounts` ENABLE KEYS */;

-- Dumping structure for table pos_db.product_transact
CREATE TABLE IF NOT EXISTS `product_transact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) NOT NULL,
  `prod_id` varchar(50) NOT NULL,
  `qty_withdrawn` varchar(50) NOT NULL,
  `unit_price` varchar(50) NOT NULL,
  `tot_cost` int(11) NOT NULL,
  `tax` double NOT NULL,
  `sold_by` varchar(150) NOT NULL,
  `customer` varchar(150) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `date_withdrawn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `employee_id` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.product_transact: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_transact` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_transact` ENABLE KEYS */;

-- Dumping structure for view pos_db.vw_user_details
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `vw_user_details` (
	`id` BIGINT(20) NOT NULL,
	`username` VARCHAR(255) NOT NULL COLLATE 'utf8_general_ci',
	`passwd` VARCHAR(100) NOT NULL COLLATE 'utf8_general_ci',
	`fullname` VARCHAR(255) NOT NULL COLLATE 'utf8_general_ci',
	`phone_number` VARCHAR(20) NOT NULL COLLATE 'utf8_general_ci',
	`login_attempt` TINYINT(1) NOT NULL,
	`status` ENUM('active','inactive','deleted','') NOT NULL COLLATE 'utf8_general_ci',
	`created_by` BIGINT(20) NOT NULL,
	`date_created` TIMESTAMP NOT NULL,
	`custom_roles` MEDIUMTEXT NULL COLLATE 'utf8_general_ci',
	`custom_privileges` MEDIUMTEXT NULL COLLATE 'utf8_general_ci',
	`group_id` VARCHAR(11) NULL COLLATE 'utf8mb4_general_ci',
	`user_roles_status` VARCHAR(8) NULL COLLATE 'utf8_general_ci',
	`group_name` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`group_roles` MEDIUMTEXT NULL COLLATE 'utf8_general_ci',
	`group_privileges` MEDIUMTEXT NULL COLLATE 'utf8_general_ci',
	`group_login_url` VARCHAR(255) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view pos_db.vw_user_details
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `vw_user_details`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_user_details` AS SELECT
    `a`.`id` AS `id`,
    `a`.`username` AS `username`,
    `a`.`passwd` AS `passwd`,
    a.fullname,
    `a`.`phone_number`,
`a`.`login_attempt` AS `login_attempt`,
`a`.`status` AS `status`,
`a`.`created_by` AS `created_by`,
`a`.`date_created` AS `date_created`,
COALESCE(`b`.`custom_roles`, '') AS `custom_roles`,
COALESCE(`b`.`custom_privileges`, '') AS `custom_privileges`,
COALESCE(`b`.`group_id`, '') AS `group_id`,
COALESCE(`b`.`status`, '') AS `user_roles_status`,
COALESCE(`c`.`name`, '') AS `group_name`,
COALESCE(`c`.`roles`, '') AS `group_roles`,
COALESCE(`c`.`privileges`, '') AS `group_privileges`,
`c`.`login_url` AS `group_login_url`
FROM
    (
        (
            (
                `access_users` `a`
            LEFT JOIN `access_roles_privileges_user` `b` ON
                ((`a`.`id` = `b`.`user_id`))
            )
        LEFT JOIN `access_roles_privileges_group` `c` ON
            ((`b`.`group_id` = `c`.`id`))
            
            
        )
    ) ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
